<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Crud extends CI_Controller
{
        public function update()
        {
                $this->load->database();
                $key = $this->input->post('key');
                        $signin_user = $this->auth->get_user();
                        $username = $this->input->post('username');
                        $password = $this->input->post('password');
                        $calendar_url = $this->input->post('calendar_url');

                        $keywords = explode("=>", $key);

                        $password = $this->encrypt->encode($password);

                $submit = $this->input->post('crud');

                if( $submit == 'Delete' )
                {
                        $this->db->delete('source_table', array('hook1' => $signin_user, 'hook3' => $keywords[0], 'hook2' => $keywords[1]));

                       // echo $this->db->last_query();
                }
                else
                {
                        $data = array('hook1' => $signin_user, 'hook3' => $username, 'hook2' => $calendar_url, 'hook4' => $password, 'hook5' => '', 'hook6' => '');

                        $this->db->where('hook1', $signin_user);
                        $this->db->where('hook3', $keywords[0]);
                        $this->db->where('hook2', $keywords[1]);

                        $this->db->update('source_table', $data);
                }

                $this->db->delete('user', array('username' => $keywords[0], 'url' => $keywords[1]));

                redirect('main');
        }
}

