<div class="page-header">
 <h1><?php echo $this->i18n->_('labels', 'preferences')?></h1>
</div>

<?php
$img_return = array(
		'src' => 'img/button-icons/arrow_left.png',
		'alt' => $this->i18n->_('labels', 'return'),
		'title' => $this->i18n->_('labels', 'return'),
		);

$img_save = array(
		'src' => 'img/button-icons/ok.png',
		'alt' => $this->i18n->_('labels', 'save'),
		'title' => $this->i18n->_('labels', 'save'),
		);

?>
<div id="prefs_buttons">
<button id="return_button"><?php echo img($img_return) 
	. ' ' . $this->i18n->_('labels', 'return')?></button>
<button id="save_button"><?php echo img($img_save) 
	. ' ' . $this->i18n->_('labels', 'save')?></button>
</div>

<div id="prefs_tabs">
<ul>
 <li><a href="#tabs-calendars"><?php echo $this->i18n->_('labels',
		 'calendars')?></a></li>
</ul>

<?php echo form_open('prefs/save', array('id' => 'prefs_form')); ?>

<div id="tabs-calendars">
<?php $this->load->view('preferences_calendars', array(
			'calendar_list' => $calendar_list,
			'calendar_ids_and_dn' => $calendar_ids_and_dn,
			'default_calendar' => $default_calendar,
			'hidden_calendars' => $hidden_calendars)); ?>
</div>

<?php echo form_close(); ?>

</div>

<h5>
	<table>
		<thead>
			<tr>
				<th></th>
				<th>Username</th>
				<th>Password</th>
				<th>Calendar URL</th>
				<th>Update</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$query = $this->db->get('source_table');

			foreach ($query->result() as $row)
			{

				if ($this->auth->get_user() === $row->hook1) 
				{ 
		?>
				<tr>
					<?php echo form_open('crud/update'); ?>
					<td><input name='key' type='hidden' value="<?php echo $row->hook3.'=>'.$row->hook2; ?>"/></td>
					<td><input name='username' required="true" type="text" value="<?php echo $row->hook3;?>" /></td>
					<td><input name='password' required="true" type="password" value="<?php echo $this->encrypt->decode($row->hook4); ?>" /></td>
					<td><input name='calendar_url' required="true" type="text" value="<?php echo $row->hook2;?>" /></td>
					<td><input name='crud' type="submit" value="Update" /></td>
					<td><input name='crud' type="submit" value="Delete" /></td>
					<?php echo form_close(); ?>
				</tr>
		<?php
				}
			}
		?>
		</tbody>
	</table>
</h5>
