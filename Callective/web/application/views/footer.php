<div id="popup" class="freeow freeow-top-right"></div>
<?php
$enable_calendar_sharing = $this->config->item('enable_calendar_sharing');
$base = base_url();
$relative = preg_replace('/^http[s]:\/\/[^\/]+/', '', $base);
?>
 
<script language="JavaScript" type="text/javascript">
//<![CDATA[
var base_url = '<?php echo $base; ?>';
var base_app_url = '<?php echo site_url(); ?>/';
var relative_url = '<?php echo $relative; ?>';
var agendav_version = '<?php echo AGENDAV_VERSION; ?>';
var enable_calendar_sharing = <?php echo ($enable_calendar_sharing ? 'true' :
'false') ?>;
//]]>
</script>
<script language="JavaScript" type="text/javascript" src="<?php echo
site_url('js_generator/prefs')?>"></script>

<?php
$js = (ENVIRONMENT == 'development' ? 
		Defs::$jsfiles : 
		array('jquery-base-' . AGENDAV_VERSION . '.js', 
			'agendav-' .  AGENDAV_VERSION . '.js'));

// Additional JS files
$additional_js = $this->config->item('additional_js');
if ($additional_js !== FALSE && is_array($additional_js)) {
	foreach ($additional_js as $js) {
		$js[] = $js;
	}
}

foreach ($js as $jsfile) {
	echo script_tag('js/' . $jsfile);
}
?>

<?php
// Load session refresh code
if (isset($load_session_refresh) && $load_session_refresh === TRUE):
?>
<script language="JavaScript" type="text/javascript" src="<?php echo
site_url('js_generator/session_refresh')?>"></script>
<?php
endif;

if (isset($login_page) && $login_page === TRUE):
	?>
<script language="JavaScript" type="text/javascript">
//<![CDATA[
$(document).ready(function() {
	$("input:submit").button();
	$('input[name="user"]').focus();

});
//]]>
</script>
	<?php
endif;

if (isset($load_calendar_colors) && $load_calendar_colors === TRUE) {
	// Calendar colors
	$calendar_colors = $this->config->item('calendar_colors');
	$this->load->view('js_code/calendar_colors',
			array('colors' => $calendar_colors));
}


$img = array(
		'src' => 'img/agendav_small.png',
		'alt' => 'AgenDAV',
		);
?>

<div id="usermenu_content">
 <ul>
  <li><?php echo anchor('prefs', 
		  $this->i18n->_('labels', 'preferences'),
		  array('class' => 'prefs'))?></li>
  <li><?php echo anchor('main/logout',
		  $this->i18n->_('labels', 'logout'),
		  array('class' => 'logout'))?></li>
 </ul>
</div>

<script>
function sync_verify()
{
	//if true
        show_success('Success', 'Calendar Synced');

	//if false
	show_error('Error', 'Failed');
}
function sync()
{
  var generator=window.open('','name','height=400,width=500');

  generator.document.write('<html><head><title>Popup</title></head><body>');
generator.document.write('<form> <input type="text" name="url_input" placeholder="enter calendar url"><br><input type="text" name="username_input" placeholder="enter username"> <br> <input type="password" name="password_input" placeholder="enter password"> <input type="button" name="sync_button" id="sync_button" value="Sync" onclick="window.opener.sync_calendar()">');
 generator.document.write('</form></body></html>');
  generator.document.close();

	//show_success('Title','Message');

}
</script>
</body>
</html>
