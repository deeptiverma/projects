﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using System.Diagnostics;
using System.Net.Mail;

using Microsoft.ServiceBus;
using System.Threading;
using WebApplication1;
namespace ConsoleApplication1
{
    class Program
    {
        public static String sub = Guid.NewGuid().ToString();

        static void Main(string[] args)
        {

            const string ConnectionString = "Endpoint=sb://deeptiservicebus.servicebus.windows.net/;SharedSecretIssuer=owner;SharedSecretValue=I1anBia8z8yvXod9LzYl9rANMz5pYeZaX71H3ICbl8c=";
            var namespaceManager = NamespaceManager.CreateFromConnectionString(ConnectionString);
            if (!namespaceManager.SubscriptionExists("TestTopic", sub))
            {
                namespaceManager.CreateSubscription("TestTopic", sub);
            }


            if (!namespaceManager.TopicExists("TestTopic"))
            {
                namespaceManager.CreateTopic("TestTopic");
            }



            var topic = TopicClient.CreateFromConnectionString(ConnectionString, "TestTopic");

            Console.WriteLine("Enter UserName");
            var userName = Console.ReadLine();
            while (true)
            {
                Console.WriteLine("Enter Message");
                var messagese = Console.ReadLine();
                var raw = new Part();
                raw.UserName = userName;
                raw.message = messagese;
                var message = new BrokeredMessage(raw);
                topic.Send(message);
            }

        }
    }

}
