﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNet.SignalR;
using Microsoft.ServiceBus.Messaging;

namespace WebApplication1
{
    using System.Diagnostics;
    using System.Threading.Tasks;

    using Microsoft.ServiceBus;

    /// <summary>
    /// The chat hub.
    /// </summary>
    public class ChatHub : Hub
    {
        public static String sub = Guid.NewGuid().ToString();

        public static List<Part> list = new List<Part>();

        public void Starting(string username) {
            //adding to the list

            if (list.Count(x => x.ConnectionID == Context.ConnectionId) == 0)
            {
                list.Add(new Part { ConnectionID = Context.ConnectionId, UserName = username });
            }
        }

        public void listenmsg() {

            const string ConnectionString = "Endpoint=sb://deeptiservicebus.servicebus.windows.net/;SharedSecretIssuer=owner;SharedSecretValue=I1anBia8z8yvXod9LzYl9rANMz5pYeZaX71H3ICbl8c=";
            var namespaceManager = NamespaceManager.CreateFromConnectionString(ConnectionString);

            if (!namespaceManager.SubscriptionExists("TestTopic", sub))
            {
                namespaceManager.CreateSubscription("TestTopic", sub);
            }
            var client = SubscriptionClient.CreateFromConnectionString(ConnectionString, "testtopic", sub);

            client.OnMessage(messagef =>
            {
                try
                {
                    var m = messagef.GetBody<Part>();
                    //  Thread.Sleep(5000);
                    int not = 1;
                    Trace.Write("out loop");
                    Trace.Write(m.UserName);

                    foreach (var a in ChatHub.list)
                    {
                        if (a.UserName.Equals(m.UserName))
                        {
                            var getconnectionId = a.ConnectionID;
                            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
                     hubContext.Clients.Client(getconnectionId).BroadcastMessage(m.UserName, m.message);
                            messagef.Complete();
                            not = 0;
                        }
                    }


                    if (not == 1)
                    {
                        messagef.Abandon();
                    }
                }
                catch (Exception ex)
                {
                    Trace.Write(ex.Message);
                    messagef.Abandon();
                }
            });
        }

        public override System.Threading.Tasks.Task OnDisconnected()
        {
            var id = Context.ConnectionId;
            list.Remove(list.Find(x => x.ConnectionID == id));
            return base.OnDisconnected();
        }

        public void Send(string name, string message)
        {
            Task.Factory.StartNew(() => QueueMessageAsync(name, message));
        }

        public async Task QueueMessageAsync(string username, string usermessage)
        {
            // sending to the topic
            const string ConnectionString = "Endpoint=sb://deeptiservicebus.servicebus.windows.net/;SharedSecretIssuer=owner;SharedSecretValue=I1anBia8z8yvXod9LzYl9rANMz5pYeZaX71H3ICbl8c=";
            var namespaceManager = NamespaceManager.CreateFromConnectionString(ConnectionString);
            if (!namespaceManager.TopicExists("TestTopic"))
            {
                namespaceManager.CreateTopic("TestTopic");
            }
           
            var topic = TopicClient.CreateFromConnectionString(ConnectionString, "TestTopic");

            // try
            // {
            var raw = new Part();

            // raw.ConnectionID = connectionId;
            raw.UserName = username;
            raw.message = usermessage;
            var message = new BrokeredMessage(raw);
            topic.Send(message);

            // }
            /*
                           finally
                            {

                                if (namespaceManager.SubscriptionExists("TestTopic", sub))
                                {
                                    namespaceManager.DeleteSubscription("TestTopic", sub);
                                }
                            }

            */

            return;
        }
    }

    public class Part : IEquatable<Part>
    {
        public string UserName { get; set; }
        public string message { get; set; }
        public String ConnectionID { get; set; }

        public override string ToString()
        {
            return "ID: " + ConnectionID + "   Name: " + UserName;
        }

        public bool Equals(Part other)
        {
            if (other == null) return false;
            return (this.ConnectionID.Equals(other.ConnectionID));
        }
    }
}