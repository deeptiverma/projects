﻿using KaryaManager.DAL;
using KaryaManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web;
using System.Web.Mvc;
using Telerik.JustMock;

namespace KaryaManager.Tests.MockHelper
{
    public class MockCreator
    {
        public UnitOfWork unitOfWork { get; set; }
        public GenericRepository<Project> projectRepository { get; set; }
        public GenericRepository<Contact> contactRepository { get; set; }
        public GenericRepository<Project_user> projectUserRepository { get; set; }
        public GenericRepository<User> userRepository { get; set; }
        public GenericRepository<Models.Task> taskRepository { get; set; }
        public IPrincipal user { get; set; }
        public HttpContextBase contextBase { get; set; }
        public ControllerContext controllerContext { get; set; }
        public WebSecurityWrapper webSecurityWrapper { get; set; }

        public void MockUser()
        {
            this.user = Mock.Create<IPrincipal>();
            this.contextBase = Mock.Create<HttpContextBase>();
            this.controllerContext = Mock.Create<ControllerContext>();
            Mock.Arrange(() => contextBase.User).Returns(user);
            Mock.Arrange(() => controllerContext.HttpContext).Returns(contextBase);
        }

        public void MockUnitOfWork()
        {
            Mock.Arrange(() => this.unitOfWork.Commit()).DoNothing();
            this.unitOfWork = Mock.Create<UnitOfWork>();
        }

        public void MockProjectRepository()
        {
            MockUnitOfWork();
            this.projectRepository = Mock.Create<GenericRepository<Project>>();
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(projectRepository);
        }

        public void MockContactRepository()
        {
            MockUnitOfWork();
            this.contactRepository = Mock.Create<GenericRepository<Contact>>();
            Mock.Arrange(() => unitOfWork.ContactRepository).Returns(contactRepository);
        }

        public void MockProjectUserRepository()
        {
            MockUnitOfWork();
            this.projectUserRepository = Mock.Create<GenericRepository<Project_user>>();
            Mock.Arrange(() => unitOfWork.ProjectUserRepository).Returns(projectUserRepository);
        }

        public void MockTaskRepository()
        {
            MockUnitOfWork();
            this.taskRepository = Mock.Create<GenericRepository<Models.Task>>();
            Mock.Arrange(() => unitOfWork.TaskRepository).Returns(taskRepository);
        }

        public void MockUserRepository()
        {
            MockUnitOfWork();
            this.userRepository = Mock.Create<GenericRepository<User>>();
            Mock.Arrange(() => unitOfWork.UserRepository).Returns(userRepository);
        }
    }
}
