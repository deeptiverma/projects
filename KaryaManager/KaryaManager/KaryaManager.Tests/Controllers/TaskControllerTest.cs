﻿using KaryaManager.Controllers;
using KaryaManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.JustMock;
using KaryaManager.DAL;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web.Security;
using KaryaManager.Tests.MockHelper;

namespace KaryaManager.Tests.Controllers
{
    [TestClass]
    public class TaskControllerTest
    {
        [TestMethod]
        public void TestIndex()
        {
            // Arrange
            var repository = Mock.Create<GenericRepository<Task>>();
            var task = new Task { taskName = "Test" };
            var unitOfWork = Mock.Create<UnitOfWork>();
            IEnumerable<Task> expected = new Task[] { task };
            Mock.Arrange(() => repository.Get(null, null, "")).Returns(expected).MustBeCalled();
            Mock.Arrange(() => unitOfWork.TaskRepository).Returns(repository).MustBeCalled();

            TaskController controller = new TaskController(unitOfWork);

            // Act
            ViewResult result = controller.Index() as ViewResult;
            var actual = (IEnumerable<Task>)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), actual.Count());
            Assert.AreEqual(expected.First(), actual.First());
        }

        [TestMethod]
        public void TestDetails()
        {
            // Arrange
            var repository = Mock.Create<GenericRepository<Task>>();
            var expected = new Task { taskName = "Test" };
            var unitOfWork = Mock.Create<UnitOfWork>();

            Mock.Arrange(() => repository.GetByID(Arg.IsAny<int>())).Returns(expected).MustBeCalled();
            Mock.Arrange(() => unitOfWork.TaskRepository).Returns(repository).MustBeCalled();

            TaskController controller = new TaskController(unitOfWork);

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            var actual = (Task)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.taskName, actual.taskName);
        }

        [TestMethod]
        public void TestDetailsInvalid()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.taskRepository.GetByID(Arg.IsAny<int>())).Returns((Task)null);
            mocker.MockTaskRepository();

            TaskController controller = new TaskController(mocker.unitOfWork);

            // Act
            HttpNotFoundResult actual = controller.Details(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestCreate()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var project = Mock.Create<Project>();
            var user = new User { userId = 0, userName = "member" };
            var owner = new User { userId = 1, userName = "owner" };
            // Mock getUserInProject()
            ICollection<Project_user> project_users = Mock.Create<ICollection<Project_user>>();
            var project_user = new Project_user { User = user };
            List<Project_user> project_users_real = new List<Project_user>();
            project_users_real.Add(project_user);
            var projectRepository = Mock.Create<GenericRepository<Project>>();

            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => project.User).Returns(owner);
            Mock.Arrange(() => project.Project_user).Returns(project_users);
            Mock.Arrange(() => projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.unitOfWork.ProjectRepository).Returns(projectRepository);
            Mock.Arrange(() => project_users.GetEnumerator()).Returns(project_users_real.GetEnumerator());

            mocker.MockTaskRepository();
            mocker.MockUser();

            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;
            var expectedStatuses = controller.getTaskStatuses();
            var expectedUsers = new List<User>();
            expectedUsers.Add(user);
            expectedUsers.Add(owner);

            // Act
            PartialViewResult result = controller.Create(1) as PartialViewResult;
            var actual = (Task)result.ViewData.Model;
            var actualStatuses = (List<String>)result.ViewBag.task_statuses;
            var actualUsers = (SelectList)result.ViewBag.assignee;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual("Open", actual.taskStatus);
            Assert.IsNotNull(actual.projectId);
            Assert.AreEqual(0, actual.progress);
            Assert.AreEqual(0, actual.priority);
            // Viewbag assert
            for (int i = 0; i < expectedStatuses.Count; i++)
            {
                Assert.AreEqual(expectedStatuses[i], actualStatuses[i]);
            }
            int index = 0;
            foreach(SelectListItem item in actualUsers)
            {
                Assert.AreEqual(expectedUsers[index].userId.ToString(), item.Value);
                Assert.AreEqual(expectedUsers[index].userName, item.Text);
                index++;
            }
        }

        [TestMethod]
        public void TestCreateNotInRole()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(false);
            mocker.MockUser();

            TaskController controller = new TaskController();
            controller.ControllerContext = mocker.controllerContext;

            // Act
            HttpNotFoundResult actual = controller.Delete(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestCreatePostValidModel()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Task expected = new Task();
            Task actual = new Task();

            Mock.Arrange(() => mocker.taskRepository.Insert(Arg.IsAny<Task>())).DoInstead((Task arg) => { actual = arg; });
            mocker.MockTaskRepository();
            TaskController controller = new TaskController(mocker.unitOfWork);

            // Act
            var result = controller.Create(expected) as RedirectToRouteResult;


            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual("TaskDashboard", result.RouteValues["action"]);
            Assert.AreEqual("Dashboard", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void TestCreatePostInValidModel()
        {
            // Arrange
            var expected = new Task { taskId = 1, projectId = 1 };
            MockCreator mocker = new MockCreator();
            var project = Mock.Create<Project>();
            var user = new User { userId = 0, userName = "member" };
            var owner = new User { userId = 1, userName = "owner" };
            // Mock getUserInProject()
            ICollection<Project_user> project_users = Mock.Create<ICollection<Project_user>>();
            var project_user = new Project_user { User = user };
            List<Project_user> project_users_real = new List<Project_user>();
            project_users_real.Add(project_user);
            var projectRepository = Mock.Create<GenericRepository<Project>>();
            Mock.Arrange(() => project_users.GetEnumerator()).Returns(project_users_real.GetEnumerator());
            Mock.Arrange(() => project.User).Returns(owner);
            Mock.Arrange(() => project.Project_user).Returns(project_users);
            Mock.Arrange(() => projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            Mock.Arrange(() => mocker.unitOfWork.ProjectRepository).Returns(projectRepository);
            mocker.MockTaskRepository();

            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ModelState.AddModelError("", "");
            var expectedStatuses = controller.getTaskStatuses();
            var expectedUsers = new List<User>();
            expectedUsers.Add(user);
            expectedUsers.Add(owner);

            // Act
            PartialViewResult result = controller.Create(expected) as PartialViewResult;
            var actual = (Task)result.ViewData.Model;
            var actualStatuses = (List<String>)result.ViewBag.task_statuses;
            var actualUsers = (SelectList)result.ViewBag.assignee;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.taskId, actual.taskId);
            for (int i = 0; i < expectedStatuses.Count; i++)
            {
                Assert.AreEqual(expectedStatuses[i], actualStatuses[i]);
            }
            int index = 0;
            foreach (SelectListItem item in actualUsers)
            {
                Assert.AreEqual(expectedUsers[index].userId.ToString(), item.Value);
                Assert.AreEqual(expectedUsers[index].userName, item.Text);
                index++;
            }
        }

        [TestMethod]
        public void TestEdit()
        {
            // Arrange 
            Task expected = new Task { taskId = 1, projectId = 1 };
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.taskRepository.GetByID(Arg.IsAny<int>())).Returns(expected);

            // Mock getUserInProjects
            var project = Mock.Create<Project>();
            var user = new User { userId = 0, userName = "member" };
            var owner = new User { userId = 1, userName = "owner" };
            ICollection<Project_user> project_users = Mock.Create<ICollection<Project_user>>();
            var project_user = new Project_user { User = user };
            List<Project_user> project_users_real = new List<Project_user>();
            project_users_real.Add(project_user);
            var projectRepository = Mock.Create<GenericRepository<Project>>();
            Mock.Arrange(() => project_users.GetEnumerator()).Returns(project_users_real.GetEnumerator());
            Mock.Arrange(() => project.User).Returns(owner);
            Mock.Arrange(() => project.Project_user).Returns(project_users);
            Mock.Arrange(() => projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            Mock.Arrange(() => mocker.unitOfWork.ProjectRepository).Returns(projectRepository);

            mocker.MockTaskRepository();
            mocker.MockUser();
            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            var actual = (Task)result.ViewData.Model;
            var actualStatuses = (List<String>)result.ViewBag.task_statuses;
            var actualUsers = (SelectList)result.ViewBag.assignee;

            // Assert
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actualStatuses);
            Assert.IsNotNull(actualUsers);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestEditNotInRole()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(false);
            mocker.MockUser();
            TaskController controller = new TaskController();
            controller.ControllerContext = mocker.controllerContext;

            // Act
            HttpNotFoundResult actual = controller.Edit(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestEditNoSuchTask()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.taskRepository.GetByID(Arg.IsAny<int>())).Returns((Task)null);
            mocker.MockUser();
            mocker.MockTaskRepository();
            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            HttpNotFoundResult actual = controller.Edit(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestEditPost()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Task expected = new Task { projectId = 1 };
            Task actual = new Task();

            Mock.Arrange(() => mocker.taskRepository.Update(Arg.IsAny<Task>())).DoInstead((Task arg) => { actual = arg; });
            mocker.MockTaskRepository();
            TaskController controller = new TaskController(mocker.unitOfWork);

            // Act
            var result = controller.Edit(expected) as RedirectToRouteResult;


            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual("TaskDashboard", result.RouteValues["action"]);
            Assert.AreEqual("Dashboard", result.RouteValues["controller"]);
            Assert.AreEqual(expected.projectId, result.RouteValues["id"]);
        }

        [TestMethod]
        public void TestEditPostInValidModel()
        {
            // Arrange
            var expected = new Task { taskId = 1, projectId = 1 };
            MockCreator mocker = new MockCreator();
            var project = Mock.Create<Project>();
            var user = new User { userId = 0, userName = "member" };
            var owner = new User { userId = 1, userName = "owner" };
            // Mock getUserInProject()
            ICollection<Project_user> project_users = Mock.Create<ICollection<Project_user>>();
            var project_user = new Project_user { User = user };
            List<Project_user> project_users_real = new List<Project_user>();
            project_users_real.Add(project_user);
            var projectRepository = Mock.Create<GenericRepository<Project>>();
            Mock.Arrange(() => project_users.GetEnumerator()).Returns(project_users_real.GetEnumerator());
            Mock.Arrange(() => project.User).Returns(owner);
            Mock.Arrange(() => project.Project_user).Returns(project_users);
            Mock.Arrange(() => projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            Mock.Arrange(() => mocker.unitOfWork.ProjectRepository).Returns(projectRepository);
            mocker.MockTaskRepository();

            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ModelState.AddModelError("", "");
            var expectedStatuses = controller.getTaskStatuses();
            var expectedUsers = new List<User>();
            expectedUsers.Add(user);
            expectedUsers.Add(owner);

            // Act
            ViewResult result = controller.Edit(expected) as ViewResult;
            var actual = (Task)result.ViewData.Model;
            var actualStatuses = (List<String>)result.ViewBag.task_statuses;
            var actualUsers = (SelectList)result.ViewBag.assignee;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.taskId, actual.taskId);
            for (int i = 0; i < expectedStatuses.Count; i++)
            {
                Assert.AreEqual(expectedStatuses[i], actualStatuses[i]);
            }
            int index = 0;
            foreach (SelectListItem item in actualUsers)
            {
                Assert.AreEqual(expectedUsers[index].userId.ToString(), item.Value);
                Assert.AreEqual(expectedUsers[index].userName, item.Text);
                index++;
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Task expected = new Task { taskId = 3, projectId = 1 };
            Task actual = new Task();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.taskRepository.GetByID(Arg.IsAny<int>())).Returns(expected);
            int actualId = 0;
            Mock.Arrange(() => mocker.taskRepository.Delete(Arg.IsAny<int>())).DoInstead((int arg) => { actualId = arg; });
            mocker.MockTaskRepository();
            mocker.MockUser();
            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            var result = controller.Delete(3) as RedirectToRouteResult;


            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.taskId, actualId);
            Assert.AreEqual("TaskDashboard", result.RouteValues["action"]);
            Assert.AreEqual("Dashboard", result.RouteValues["controller"]);
            Assert.AreEqual(expected.projectId, result.RouteValues["id"]);

        }

        [TestMethod]
        public void TestDeleteNotInRole()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(false);
            mocker.MockUser();
            TaskController controller = new TaskController();
            controller.ControllerContext = mocker.controllerContext;

            // Act
            HttpNotFoundResult actual = controller.Delete(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestUpdateStatus()
        {
            // Arrange
            var expectedProject = new Task { taskStatus = "Open", taskId = 1 };
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            string expected = "In Progress";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.taskRepository.GetByID(Arg.IsAny<int>())).Returns(expectedProject);
            string actual = "";
            Mock.Arrange(() => mocker.taskRepository.Update(Arg.IsAny<Task>())).DoInstead((Task arg) => actual = arg.taskStatus);
            mocker.MockUser();
            mocker.MockTaskRepository();
            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            controller.UpdateStatus(expected, 1);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestUpdateUser()
        {
            // Arrange
            var expectedProject = new Task { taskStatus = "Open", taskId = 1 };
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            int expected = 3;
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.taskRepository.GetByID(Arg.IsAny<int>())).Returns(expectedProject);
            int? actual = 0;
            Mock.Arrange(() => mocker.taskRepository.Update(Arg.IsAny<Task>())).DoInstead((Task arg) => actual = arg.assignee);
            mocker.MockUser();
            mocker.MockTaskRepository();
            TaskController controller = new TaskController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            controller.UpdateUser(expected, 1);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestGetUserInProject()
        {
            // Arrange
            MockCreator mocker = new MockCreator();

            // Mock getUserInProjects
            var project = Mock.Create<Project>();
            var user = new User { userId = 0, userName = "member" };
            var owner = new User { userId = 1, userName = "owner" };
            ICollection<Project_user> project_users = Mock.Create<ICollection<Project_user>>();
            var project_user = new Project_user { User = user };
            List<Project_user> project_users_real = new List<Project_user>();
            project_users_real.Add(project_user);
            Mock.Arrange(() => project_users.GetEnumerator()).Returns(project_users_real.GetEnumerator());
            Mock.Arrange(() => project.User).Returns(owner);
            Mock.Arrange(() => project.Project_user).Returns(project_users);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            mocker.MockProjectRepository();
            mocker.MockUser();

            List<User> expected = new List<User> { user, owner };

            TaskController controller = new TaskController(mocker.unitOfWork);

            // Act
            var actual = controller.getUserInProject(1);

            // Assert
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
        }

        [TestMethod]
        public void TestGetTaskStatuses()
        {
            // Arrange
            TaskController controller = new TaskController();

            // Act
            List<String> actual = controller.getTaskStatuses();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Count);
        }

    }
}
