﻿using KaryaManager.Controllers;
using KaryaManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.JustMock;
using KaryaManager.DAL;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web.Security;
using KaryaManager.Tests.MockHelper;

namespace KaryaManager.Tests.Controllers
{
    [TestClass]
    public class ProjectUserControllerTest
    {
        [TestMethod]
        public void TestIndex()
        {
            MockCreator mocker = new MockCreator();
            var project = Mock.Create<Project>();
            var user = new User { userId = 1 };
            var project_user = new Project_user { User = user };
            ICollection<Project_user> expected = new List<Project_user> { project_user };

            Mock.Arrange(() => project.Project_user).Returns(expected);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            mocker.MockProjectRepository();
            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork);

            // Act
            PartialViewResult result = controller.Index() as PartialViewResult;
            var actual = (ICollection<Project_user>)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCreate()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var user = new User { userName = "Test", userId = 1};
            var contact = new Contact { User1 = user };
            var expected = new Project_user { projectId = 1 };
            var project = new Project { Project_user = new List<Project_user>() };
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            var projectRepository = Mock.Create<GenericRepository<Project>>();
            ICollection<Contact> contacts = new List<Contact> { contact };
            var currentUser = new User { Contacts = contacts };
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);
            Mock.Arrange(() => mocker.userRepository.GetByID(Arg.IsAny<int>())).Returns(currentUser);
            Mock.Arrange(() => projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            Mock.Arrange(() => mocker.unitOfWork.ProjectRepository).Returns(projectRepository);
            mocker.MockUser();
            mocker.MockUserRepository();

            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork, webSecurity);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            PartialViewResult result = controller.Create(1) as PartialViewResult;
            var actualUser = (SelectList)result.ViewBag.userId;
            var actual = (Project_user)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            foreach (SelectListItem item in actualUser)
            {
                Assert.AreEqual(user.userName, item.Text);
                Assert.AreEqual(user.userId.ToString(), item.Value);
            }
           // Assert.Equals(user.userName, actualUser.items
        }

        [TestMethod]
        public void TestCreatePostValidModel()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            string actualUserName = "", actualRole = "";
            var expectedRoleName = "1 Member";
            Project_user expected = new Project_user { projectId = 1 };
            Mock.Arrange(() => Roles.AddUserToRole(Arg.IsAny<string>(), Arg.IsAny<string>())).DoInstead((string arg1, string arg2) =>
            {
                actualUserName = arg1;
                actualRole = arg2;
            });
            Project_user actual = null;
            Mock.Arrange(() => mocker.projectUserRepository.Insert(Arg.IsAny<Project_user>())).DoInstead((Project_user arg1) =>
            {
                actual = arg1;
            });
            var userRepository = Mock.Create<GenericRepository<User>>();
            var user = new User { userName = "test" };
            Mock.Arrange(() => userRepository.GetByID(Arg.IsAny<int>())).Returns(user);
            Mock.Arrange(() => mocker.unitOfWork.UserRepository).Returns(userRepository);
            mocker.MockProjectUserRepository();

            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork);

            // Act
            var result = controller.Create(expected) as RedirectToRouteResult;
            

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(user.userName, actualUserName);
            Assert.AreEqual(expectedRoleName, actualRole);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Dashboard", result.RouteValues["controller"]);
            Assert.AreEqual(expected.projectId, result.RouteValues["id"]);

        }

        [TestMethod]
        public void TestCreatePostInvalidModel()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var user = new User { userName = "Test", userId = 1 };
            var contact = new Contact { User1 = user };
            var expected = new Project_user { projectId = 1 };
            var project = new Project { Project_user = new List<Project_user>() };
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            var projectRepository = Mock.Create<GenericRepository<Project>>();
            ICollection<Contact> contacts = new List<Contact> { contact };
            var currentUser = new User { Contacts = contacts };
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);
            Mock.Arrange(() => mocker.userRepository.GetByID(Arg.IsAny<int>())).Returns(currentUser);
            Mock.Arrange(() => projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            Mock.Arrange(() => mocker.unitOfWork.ProjectRepository).Returns(projectRepository);
            mocker.MockUser();
            mocker.MockUserRepository();

            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork, webSecurity);
            controller.ControllerContext = mocker.controllerContext;
            controller.ModelState.AddModelError("", "");


            // Act
            PartialViewResult result = controller.Create(expected) as PartialViewResult;
            var actualUser = (SelectList)result.ViewBag.userId;
            var actual = (Project_user)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
            foreach (SelectListItem item in actualUser)
            {
                Assert.AreEqual(user.userName, item.Text);
                Assert.AreEqual(user.userId.ToString(), item.Value);
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            MockCreator mocker = new MockCreator();
            var user = new User { userName = "Test", userId = 1 };
            var expected = new Project_user { projectId = 1, project_userId = 1 , User = user};
            string actualUserName = "", actualRole = "";
            var expectedRoleName = "1 Member";
            Mock.Arrange(() => Roles.RemoveUserFromRole(Arg.IsAny<string>(), Arg.IsAny<string>())).DoInstead((string arg1, string arg2) =>
            {
                actualUserName = arg1;
                actualRole = arg2;
            });
            int actual = 0;
            Mock.Arrange(() => mocker.projectUserRepository.GetByID(Arg.IsAny<int>())).Returns(expected);
            Mock.Arrange(() => mocker.projectUserRepository.Delete(Arg.IsAny<int>())).DoInstead((int arg) =>
            {
                actual = arg;
            });
            mocker.MockProjectUserRepository();

            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork);


           // Act
            var result = controller.Delete(1) as RedirectToRouteResult;


            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.project_userId, actual);
            Assert.AreEqual(user.userName, actualUserName);
            Assert.AreEqual(expectedRoleName, actualRole);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Dashboard", result.RouteValues["controller"]);
            Assert.AreEqual(expected.projectId, result.RouteValues["id"]);
        }

        [TestMethod]
        public void TestDeleteNoSuchProjectUser()
        {
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.projectUserRepository.GetByID(Arg.IsAny<int>())).Returns((Project_user)null);
            mocker.MockProjectUserRepository();

            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork);


            // Act
            var result = controller.Delete(1) as HttpNotFoundResult;


            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void TestGetUserInContact(int projectId)
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            // Mock getUserInProjects
            // Setup a user to already be in the project user list
            var project = Mock.Create<Project>();
            var user = new User { userId = 0, userName = "member" };
            var owner = new User { userId = 1, userName = "owner" };
            ICollection<Project_user> project_users = Mock.Create<ICollection<Project_user>>();
            var project_user = new Project_user { User = user };
            List<Project_user> project_users_real = new List<Project_user>();
            project_users_real.Add(project_user);
            Mock.Arrange(() => project_users.GetEnumerator()).Returns(project_users_real.GetEnumerator());
            Mock.Arrange(() => project.User).Returns(owner);
            Mock.Arrange(() => project.Project_user).Returns(project_users);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);

            // Mock user repository
            // Setup another user in the contact, but not in the project
            var user1 = new User { userId = 1, userName = "New member" };
            var contact1 = new Contact { User1 = user };
            var contact2 = new Contact { User1 = user1 };
            ICollection<Contact> contacts = new List<Contact> { contact1, contact2 };
            var currentUser = new User { Contacts = contacts };
            var userRepository = Mock.Create<GenericRepository<User>>();
            Mock.Arrange(() => userRepository.GetByID(Arg.IsAny<int>())).Returns(currentUser);
            Mock.Arrange(() => mocker.unitOfWork.UserRepository).Returns(userRepository);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);


            mocker.MockProjectRepository();

            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork, webSecurity);

            // Act
            List<User> actual = controller.getUserInContact(1);

            // Assert
            // Assert that just the user not in the project, but on contact is returned in the list
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(user1, actual[0]);

        }

        [TestMethod]
        public void TestGetUserInProject(int? projectId)
        {
            // Arrange
            MockCreator mocker = new MockCreator();

            // Mock getUserInProjects
            var project = Mock.Create<Project>();
            var user = new User { userId = 0, userName = "member" };
            var owner = new User { userId = 1, userName = "owner" };
            ICollection<Project_user> project_users = Mock.Create<ICollection<Project_user>>();
            var project_user = new Project_user { User = user };
            List<Project_user> project_users_real = new List<Project_user>();
            project_users_real.Add(project_user);
            Mock.Arrange(() => project_users.GetEnumerator()).Returns(project_users_real.GetEnumerator());
            Mock.Arrange(() => project.User).Returns(owner);
            Mock.Arrange(() => project.Project_user).Returns(project_users);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            mocker.MockProjectRepository();
            mocker.MockUser();

            List<User> expected = new List<User> { user, owner };

            ProjectUserController controller = new ProjectUserController(mocker.unitOfWork);

            // Act
            var actual = controller.getUserInProject(1);

            // Assert
            Assert.AreEqual(2, actual.Count);
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
        }

        [TestMethod]
        public void TestAddUserToTeamRole()
        {
            // Arrange
            string expectedName = "Test";
            string expectedRole = "1 Member";
            string actualName = "";
            string actualRole = "";
            Mock.Arrange(() => Roles.AddUserToRole(Arg.IsAny<string>(), Arg.IsAny<string>())).DoInstead((string arg1, string arg2) =>
            {
                actualName = arg1;
                actualRole = arg2;
            });
            ProjectUserController controller = new ProjectUserController();

            // Act
            controller.AddUserToTeamRole(1, expectedName);

            // Assert
            Assert.AreEqual(expectedName, actualName);
            Assert.AreEqual(expectedRole, actualRole);
        }

        [TestMethod]
        public void TestRemoveUserFromTeamRole()
        {
            // Arrange
            string expectedName = "Test";
            string expectedRole = "1 Member";
            string actualName = "";
            string actualRole = "";
            Mock.Arrange(() => Roles.RemoveUserFromRole(Arg.IsAny<string>(), Arg.IsAny<string>())).DoInstead((string arg1, string arg2) =>
            {
                actualName = arg1;
                actualRole = arg2;
            });
            ProjectUserController controller = new ProjectUserController();

            // Act
            controller.RemoveUserFromTeamRole(1, expectedName);

            // Assert
            Assert.AreEqual(expectedName, actualName);
            Assert.AreEqual(expectedRole, actualRole);
        }

    }
}
