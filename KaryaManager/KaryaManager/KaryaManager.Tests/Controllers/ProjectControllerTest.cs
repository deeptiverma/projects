﻿using KaryaManager.Controllers;
using KaryaManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.JustMock;
using Telerik.JustMock.Container;
using KaryaManager.DAL;
using System.Security.Principal;
using System.Web;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web.Security;
using KaryaManager.Tests.MockHelper;
    
namespace KaryaManager.Tests.Controllers
{
    [TestClass]
    public class ProjectControllerTest
    {
        [TestMethod]
        public void TestIndex()
        {
            // Arrange
            var repository = Mock.Create<GenericRepository<Project>>();
            var project = new Project { projectName = "Test" };
            var unitOfWork = Mock.Create<UnitOfWork>();
            IEnumerable<Project> expected = new Project[] { project };
            Mock.Arrange(() => repository.Get(null, null, "")).Returns(expected).MustBeCalled();
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(repository).MustBeCalled();

            ProjectController controller = new ProjectController(unitOfWork);

            // Act
            ViewResult result = controller.Index() as ViewResult;
            var actual = (IEnumerable<Project>)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count(), actual.Count());
            Assert.AreEqual(expected.First(), actual.First());
        }

        [TestMethod]
        public void TestDetails()
        {
            // Arrange
            var repository = Mock.Create<GenericRepository<Project>>();
            var expected = new Project { projectName = "Test" };
            var unitOfWork = Mock.Create<UnitOfWork>();

            Mock.Arrange(() => repository.GetByID(Arg.IsAny<int>())).Returns(expected).MustBeCalled();
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(repository).MustBeCalled();

            ProjectController controller = new ProjectController(unitOfWork);

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            var actual = (Project)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.projectName, actual.projectName);
        }

        [TestMethod]
        public void TestDetailsInvalid()
        {
            // Arrange
            var repository = Mock.Create<GenericRepository<Project>>();
            var expected = new Project { projectName = "Test" };
            var unitOfWork = Mock.Create<UnitOfWork>();

            Mock.Arrange(() => repository.GetByID(Arg.IsAny<int>())).Returns((Project)null);
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(repository);

            ProjectController controller = new ProjectController(unitOfWork);

            // Act
            HttpNotFoundResult actual = controller.Details(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestCreate()
        {
            // Arrange
            ProjectController controller = new ProjectController();
            var expectedStatuses = controller.getProjectStatuses();

            // Act
            PartialViewResult result = controller.Create() as PartialViewResult;
            var actual = result.ViewData.Model;
            var actualStatuses = (List<String>)result.ViewBag.project_statuses;

            // Assert
            Assert.IsNotNull(actual);
            for (int i = 0; i < expectedStatuses.Count; i++)
            {
                Assert.AreEqual(expectedStatuses[i], actualStatuses[i]);
            }

        }

        [TestMethod]
        public void TestCreatePostValidModel()
        {
            // Arrange
            int userId = 1;
            String userName = "test";
            var repository = Mock.Create<GenericRepository<Project>>();
            var expected = new Project { projectName = "Test", projectId=1 };
            var unitOfWork = Mock.Create<UnitOfWork>();
            var actual = new Project();
            var webSecurityMock = Mock.Create<WebSecurityWrapper>();

            Mock.Arrange(() => Roles.CreateRole(Arg.IsAny<string>())).DoNothing();
            Mock.Arrange(() => Roles.AddUserToRole(Arg.IsAny<string>(), Arg.IsAny<string>())).DoNothing();
            Mock.Arrange(() => repository.Insert(Arg.IsAny<Project>())).DoInstead((Project arg1) => { actual = arg1; });
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(repository).MustBeCalled();
            Mock.Arrange(() => webSecurityMock.CurrentUserId).Returns(userId);
            Mock.Arrange(() => webSecurityMock.CurrentUserName).Returns(userName);

            ProjectController controller = new ProjectController(unitOfWork, webSecurityMock);

            // Act
            controller.Create(expected);
            var result = controller.Create(expected) as RedirectToRouteResult;
            

            // Assert
            Assert.IsNotNull(actual);
            

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual, expected);
            Assert.AreEqual(actual.projectId, expected.projectId);
            Assert.AreEqual(actual.projectName, expected.projectName);
            Assert.AreEqual(actual.ownerId, userId);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Workspace", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void TestCreatePostInValidModel()
        {
            // Arrange
            var expected = new Project { projectId = 1 };
            ProjectController controller =  new ProjectController();
            var expectedStatuses = new SelectList(controller.getProjectStatuses()).ToArray();

            controller.ModelState.AddModelError("", "");

            // Act
            controller.Create(expected);

            PartialViewResult result = controller.Create(expected) as PartialViewResult;
            var actual = (Project)result.ViewData.Model;
            var actualStatuses = ((SelectList)result.ViewBag.project_statuses).ToArray();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
            for (int i = 0; i < expectedStatuses.Length; i++)
            {
                Assert.AreEqual(expectedStatuses[i].Text, actualStatuses[i].Text);
            }

        }

        [TestMethod]
        public void TestEdit()
        {
            // Arrange
            CurrentRole.leaderRole = "Leader";
            var userMock = Mock.Create<IPrincipal>();
            var contextMock = Mock.Create<HttpContextBase>();
            var controllerContextMock = Mock.Create<ControllerContext>();
            var repository = Mock.Create<GenericRepository<Project>>();
            var expected = new Project { projectName = "Test" };
            var unitOfWork = Mock.Create<UnitOfWork>();

            Mock.Arrange(() => userMock.IsInRole("Leader")).Returns(true);
            Mock.Arrange(() => contextMock.User).Returns(userMock);            
            Mock.Arrange(() => controllerContextMock.HttpContext).Returns(contextMock);
            Mock.Arrange(() => repository.GetByID(Arg.IsAny<int>())).Returns(expected);
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(repository).MustBeCalled();

            ProjectController controller = new ProjectController(unitOfWork);
            controller.ControllerContext = controllerContextMock;

            // Act
            PartialViewResult result = controller.Edit(1) as PartialViewResult;
            var actual = (Project)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.projectName, actual.projectName);
        }

        [TestMethod]
        public void TestEditNotInRole()
        {
            // Arrange
            CurrentRole.leaderRole = "Leader";
            var userMock = Mock.Create<IPrincipal>();
            var contextMock = Mock.Create<HttpContextBase>();
            var controllerContextMock = Mock.Create<ControllerContext>();

            Mock.Arrange(() => userMock.IsInRole("Leader")).Returns(false);
            Mock.Arrange(() => contextMock.User).Returns(userMock);
            Mock.Arrange(() => controllerContextMock.HttpContext).Returns(contextMock);
            

            ProjectController controller = new ProjectController();
            controller.ControllerContext = controllerContextMock;

            // Act
            HttpNotFoundResult actual = controller.Edit(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestEditNoSuchProject()
        {
            // Arrange
            CurrentRole.leaderRole = "Leader";
            var userMock = Mock.Create<IPrincipal>();
            var contextMock = Mock.Create<HttpContextBase>();
            var controllerContextMock = Mock.Create<ControllerContext>();
            var repository = Mock.Create<GenericRepository<Project>>();
            var unitOfWork = Mock.Create<UnitOfWork>();

            Mock.Arrange(() => userMock.IsInRole("Leader")).Returns(true);
            Mock.Arrange(() => contextMock.User).Returns(userMock);
            Mock.Arrange(() => controllerContextMock.HttpContext).Returns(contextMock);
            Mock.Arrange(() => repository.GetByID(Arg.IsAny<int>())).Returns((Project)null);
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(repository).MustBeCalled();

            ProjectController controller = new ProjectController(unitOfWork);
            controller.ControllerContext = controllerContextMock;

            // Act
            HttpNotFoundResult actual = controller.Edit(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestEditPost()
        {
            // Arrange
            var repository = Mock.Create<GenericRepository<Project>>();
            var unitOfWork = Mock.Create<UnitOfWork>();
            var expected = new Project { projectId = 1, projectStatus = "Open" };
            var actual = new Project();
            Mock.Arrange(() => repository.Update(Arg.IsAny<Project>())).DoInstead((Project arg) => { actual = arg; });
            Mock.Arrange(() => unitOfWork.ProjectRepository).Returns(repository).MustBeCalled();

            // Act
            ProjectController controller = new ProjectController(unitOfWork);
            var result = controller.Edit(expected) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual, expected);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Dashboard", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void TestEditPostInvalidModel()
        {
            // Arrange
            var expected = new Project { projectId = 1, projectStatus = "Open" };

            ProjectController controller = new ProjectController();
            controller.ModelState.AddModelError("", "");

            // Act
            PartialViewResult result = controller.Edit(expected) as PartialViewResult;
            var actual = result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void TestDelete()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => Roles.RemoveUsersFromRole(Arg.IsAny<string[]>(), Arg.IsAny<string>())).DoNothing();
            Mock.Arrange(() => Roles.DeleteRole(Arg.IsAny<string>())).DoNothing();
            string[] temp = new string[] { "test" };
            Mock.Arrange(() => Roles.GetUsersInRole(Arg.IsAny<string>())).Returns(temp);
            Mock.Arrange(() => Roles.GetUsersInRole(Arg.IsAny<string>())).Returns(temp);
            mocker.MockUser();
            Mock.Arrange(() => mocker.projectRepository.Delete(Arg.IsAny<int>())).Returns(true);
            mocker.MockProjectRepository();
            ProjectController controller = new ProjectController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            RedirectToRouteResult actual = controller.Delete(1) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual("Index", actual.RouteValues["action"]);
            Assert.AreEqual("Workspace", actual.RouteValues["controller"]);
        }

        [TestMethod]
        public void TestDeleteNotInRole()
        {
            // Arrange 
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(false);
            mocker.MockUser();
            ProjectController controller = new ProjectController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            HttpNotFoundResult actual = controller.Delete(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestDeleteNotFound()
        {
            // Arrange 
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(false);
            mocker.MockUser();
            Mock.Arrange(() => mocker.projectRepository.Delete(Arg.IsAny<int>())).Returns(false);
            ProjectController controller = new ProjectController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            HttpNotFoundResult actual = controller.Delete(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestGetProjectStatuses()
        {
            // Arrange
            ProjectController controller = new ProjectController();

            // Act
            List<String> actual = controller.getProjectStatuses();

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(4, actual.Count);
        }

        [TestMethod]
        public void TestEstablishProjectRole()
        {
            // Arrange
            int projectId = 1;
            string expectedRole = 1 + " Leader";
            string expectedUserName = "Test";
            WebSecurityWrapper wr = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => wr.CurrentUserName).Returns(expectedUserName);
            Mock.Arrange(() => Roles.CreateRole(Arg.IsAny<string>())).DoNothing();
            var actualUserName = "";
            var actualRole = "";
            Mock.Arrange(() => Roles.AddUserToRole(Arg.IsAny<string>(), Arg.IsAny<string>())).DoInstead((string arg, string arg2) => { actualUserName = arg; actualRole = arg2; });
            ProjectController controller = new ProjectController(null, wr);

            // Act
            controller.establishProjectRole(1);

            // Assert
            Assert.AreEqual(expectedRole, actualRole);
            Assert.AreEqual(expectedUserName, actualUserName);
        }

        [TestMethod]
        public void TestDeleteProjectRole()
        {
            // Arrange
            List<string> expected = new List<string>();
            expected.Add("1 Leader");
            expected.Add("1 Member");
            string [] expectedMember = new string[] { "member1" };
            string [] expectedLeader = new string[] { "leader1"};
            string [][] expectedUser = new string[2][];
            expectedUser[0] = expectedLeader;
            expectedUser[1] = expectedMember;
            List<string> actual = new List<string>();
            Mock.Arrange(() => Roles.GetUsersInRole(expected[0])).Returns(expectedLeader);
            Mock.Arrange(() => Roles.GetUsersInRole(expected[1])).Returns(expectedMember);
            List<string []> actualUser = new List<string[]>();
            Mock.Arrange(() => Roles.RemoveUsersFromRole(Arg.IsAny<string[]>(), Arg.IsAny<string>())).DoInstead((string[] arg0, string arg1) => {
                actualUser.Add(arg0);
            });
            Mock.Arrange(() => Roles.DeleteRole(Arg.IsAny<string>())).DoInstead((string arg) => { actual.Add(arg); });

            ProjectController controller = new ProjectController();

            // Act
            controller.deleteProjectRole(1);
            
            // Assert
            Assert.AreEqual(expected.Count, actual.Count);
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
            Assert.AreEqual(2, actualUser.Count);
            for (int i = 0; i < actualUser.Count; i++)
            {
                Assert.AreEqual(expectedUser[i][0], actualUser[i][0]);
            }
        }

        [TestMethod]
        public void TestDeletePrompt()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => Roles.RemoveUsersFromRole(Arg.IsAny<string[]>(), Arg.IsAny<string>())).DoNothing();
            Mock.Arrange(() => Roles.DeleteRole(Arg.IsAny<string>())).DoNothing();
            string[] temp = new string[] { "test" };
            Mock.Arrange(() => Roles.GetUsersInRole(Arg.IsAny<string>())).Returns(temp);
            Mock.Arrange(() => Roles.GetUsersInRole(Arg.IsAny<string>())).Returns(temp);
            mocker.MockUser();
            Mock.Arrange(() => mocker.projectRepository.Delete(Arg.IsAny<int>())).Returns(true);
            mocker.MockProjectRepository();
            ProjectController controller = new ProjectController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Act
            controller.DeletePrompt(1);

            // Assert
            RedirectToRouteResult actual = controller.Delete(1) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual("Index", actual.RouteValues["action"]);
            Assert.AreEqual("Workspace", actual.RouteValues["controller"]);
        }

    }
}
