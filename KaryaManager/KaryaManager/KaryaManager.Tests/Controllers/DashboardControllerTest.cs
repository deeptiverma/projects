﻿using KaryaManager.Controllers;
using KaryaManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.JustMock;
using KaryaManager.DAL;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web.Security;
using KaryaManager.Tests.MockHelper;

namespace KaryaManager.Tests.Controllers
{
    [TestClass]
    public class DashboardControllerTest
    {
        [TestMethod]
        public void IndexTeamLeader()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.Matches<string>(x => x == CurrentRole.leaderRole))).Returns(true);
            Mock.Arrange(() => mocker.user.IsInRole(Arg.Matches<string>(x => x == CurrentRole.memberRole))).Returns(false);
            mocker.MockUser();

            DashboardController controller = new DashboardController();
            controller.ControllerContext = mocker.controllerContext;

            // Arrange
            var actual = controller.Index(1) as ViewResult;
            var actualId = actual.ViewBag.projectId;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actualId);
        }

        [TestMethod]
        public void IndexTeamMember()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.Matches<string>(x => x == CurrentRole.leaderRole))).Returns(false);
            Mock.Arrange(() => mocker.user.IsInRole(Arg.Matches<string>(x => x == CurrentRole.memberRole))).Returns(true);
            mocker.MockUser();

            DashboardController controller = new DashboardController();
            controller.ControllerContext = mocker.controllerContext;

            // Arrange
            var result = controller.Index(1) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("TaskDashboard", result.RouteValues["action"]);
            Assert.AreEqual(1, result.RouteValues["id"]);
        }

        [TestMethod]
        public void IndexInvalid()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(false);
            mocker.MockUser();
            DashboardController controller = new DashboardController();
            controller.ControllerContext = mocker.controllerContext;

            // Act
            var result = controller.Index(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void TestProjectDashboard()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var expected = new Project();
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(expected);
            mocker.MockProjectRepository();

            DashboardController controller = new DashboardController(mocker.unitOfWork);

            // Arrange
            var result = controller.ProjectDashboard(1) as PartialViewResult;
            var actual = (Project)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestProjectDashboardNotFound()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns((Project)null);
            mocker.MockProjectRepository();

            DashboardController controller = new DashboardController(mocker.unitOfWork);

            // Arrange
            var result = controller.ProjectDashboard(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void TestTaskDashboard()
        {
            // Arrange
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            MockCreator mocker = new MockCreator();
            var expected = new Project { Tasks = new List<Task> { new Task() } };
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(expected);
            mocker.MockProjectRepository();
            mocker.MockUser();

            DashboardController controller = new DashboardController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Arrange
            var result = controller.TaskDashboard(1) as ViewResult;
            var actual = (List<Task>)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expected.Tasks, actual);
        }

        [TestMethod]
        public void TestTaskDashboardNotInRole()
        {
            // Arrange
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(false);
            mocker.MockUser();

            DashboardController controller = new DashboardController();
            controller.ControllerContext = mocker.controllerContext;

            // Arrange
            var result = controller.TaskDashboard(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void TestTaskDashboardNotFound()
        {
            // Arrange
            CurrentRole.leaderRole = "leader";
            CurrentRole.memberRole = "member";
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns((Project)null);
            mocker.MockProjectRepository();
            mocker.MockUser();

            DashboardController controller = new DashboardController(mocker.unitOfWork);
            controller.ControllerContext = mocker.controllerContext;

            // Arrange
            var result = controller.TaskDashboard(1) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public void TestGetTaskStatuses()
        {
            // Arrange
            DashboardController controller = new DashboardController();

            // Act
            var actual = controller.getTaskStatuses(); 

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Count);
        }

        [TestMethod]
        public void TestGetUserInProject()
        {
            var user1 = new User();
            var user2 = new User();
            var userOwner = new User();
            var project_user1 = new Project_user { User = user1 };
            var project_user2 = new Project_user { User = user2 };
            List<User> expected = new List<User> { user1, user2, userOwner };
            var project_users = new List<Project_user> { project_user1, project_user2 };
            var project = new Project { Project_user = project_users, User = userOwner };
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.user.IsInRole(Arg.IsAny<string>())).Returns(true);
            Mock.Arrange(() => mocker.projectRepository.GetByID(Arg.IsAny<int>())).Returns(project);
            mocker.MockProjectRepository();

            DashboardController controller = new DashboardController(mocker.unitOfWork);

            // Arrange
            List<User> actual = controller.getUserInProject(1);

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected.Count, actual.Count);
            Assert.AreEqual(expected[0], actual[0]);
            Assert.AreEqual(expected[1], actual[1]);
            Assert.AreEqual(expected[2], actual[2]);
        }

        [TestMethod]
        public void TestGetCurrentTaskStatus()
        {
            // Arrange
            Task task1 = new Task { taskStatus = "Open" };
            Task task2 = new Task { taskStatus = "Closed" };
            Task task3 = new Task { taskStatus = "In Progress" };
            Project project = new Project { Tasks = new List<Task> { task1, task2, task3 } };

            DashboardController controller = new DashboardController();

            // Act
            var actual = controller.getCurrentTaskStatus(project);

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(3, actual.Length);
            Assert.AreEqual(1, actual[0]);
            Assert.AreEqual(1, actual[1]);
            Assert.AreEqual(1, actual[2]);
        }

        [TestMethod]
        public void TestGetCurrentTaskPriorities()
        {
            // Arrange
            Task task1 = new Task { priority = 1 };
            Task task2 = new Task { priority = 2 };
            Task task3 = new Task { priority = 1 };
            Project project = new Project { Tasks = new List<Task> { task1, task2, task3 } };

            DashboardController controller = new DashboardController();

            // Act
            var actual = controller.getCurrentTaskPriorities(project);

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(6, actual.Length);
            Assert.AreEqual(0, actual[0]);
            Assert.AreEqual(2, actual[1]);
            Assert.AreEqual(1, actual[2]);
            Assert.AreEqual(0, actual[3]);
            Assert.AreEqual(0, actual[4]);
            Assert.AreEqual(0, actual[5]);
        }
    }
}
