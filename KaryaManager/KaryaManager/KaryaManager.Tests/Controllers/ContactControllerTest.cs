﻿using KaryaManager.Controllers;
using KaryaManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.JustMock;
using KaryaManager.DAL;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web.Security;
using KaryaManager.Tests.MockHelper;

namespace KaryaManager.Tests.Controllers
{
    [TestClass]
    public class ContactControllerTest
    {
        [TestMethod]
        public void TestIndex()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);
            var expected = new Contact ();
            var contacts = new List<Contact> { expected };
            var user = new User{ Contacts = contacts };
            Mock.Arrange(() => mocker.userRepository.GetByID(Arg.IsAny<int>())).Returns(user);
            mocker.MockUserRepository();
            ContactController controller = new ContactController(mocker.unitOfWork, webSecurity);

            // Act
            ViewResult result = controller.Index() as ViewResult;
            var actual = (List<Contact>)result.ViewData.Model;
            
            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(expected, actual[0]);
        }

        [TestMethod]
        public void TestCreate()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);

            // Setup current user UserA, UserB who is not in contact list, UserC who is in contact, UserD who is in contact with userc
            var userA = new User { userId = 1 };
            var userB = new User { userId = 2 };
            var userC = new User { userId = 3 };
            var userD = new User { userId = 4 };

            var contactAToB = new Contact { primary_userId = 1, secondary_userId = 2 };
            var contactDtoC = new Contact { primary_userId = 4, secondary_userId = 3 };
            userA.Contacts = new List<Contact>{ contactAToB };
            userB.Contacts1 = new List<Contact> { contactAToB };
            userC.Contacts1 = new List<Contact>{ contactDtoC };
            userD.Contacts = new List<Contact> { contactDtoC };

            // mock all user return
            var allUsers = new List<User> { userA, userB, userC, userD };
            Mock.Arrange(() => mocker.userRepository.Get(null, null, "")).Returns(allUsers);

            // mock return all contact
            var allContacts = new List<Contact> { contactAToB, contactDtoC };
            var contactRepository = Mock.Create<GenericRepository<Contact>>();
            Mock.Arrange(() => contactRepository.Get(null, null, "")).Returns(allContacts);
            Mock.Arrange(() => mocker.unitOfWork.ContactRepository).Returns(contactRepository);
            mocker.MockUserRepository();

            ContactController controller = new ContactController(mocker.unitOfWork, webSecurity);

            // Act
            PartialViewResult result = controller.Create() as PartialViewResult;
            var actual = (Contact)result.ViewData.Model;
            var actualUser = (SelectList)result.ViewBag.secondary_user;

            // Assert
            Assert.IsNotNull(actual);
            // Only userc and userd is not in user A contact, so those are what the viewbag passed to the view
            Assert.AreEqual(userC.userId.ToString(), actualUser.ElementAt(0).Value);
            Assert.AreEqual(userD.userId.ToString(), actualUser.ElementAt(1).Value);
        }

        [TestMethod]
        public void TestCreatePostValidModel()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);
            Contact expected = new Contact();
            Contact actual = null;
            Mock.Arrange(() => mocker.contactRepository.Insert(Arg.IsAny<Contact>())).DoInstead((Contact arg) =>
            {
                actual = arg;
            });
            mocker.MockContactRepository();

            ContactController controller = new ContactController(mocker.unitOfWork, webSecurity);

            // Act
            var result = controller.Create(expected) as RedirectToRouteResult;


            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual("Workspace", result.RouteValues["controller"]);
        }

        [TestMethod]
        public void TestCreatePostInvalidModel()
        {
            // Arrange
            Contact expected = new Contact();
            ContactController controller = new ContactController();
            controller.ModelState.AddModelError("", "");

            // Act
            var result = controller.Create(expected) as PartialViewResult;
            var actual = (Contact)result.ViewData.Model;


            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestDelete()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Contact expected = new Contact { contact_id = 1 };
            int actual = 0;
            Mock.Arrange(() => mocker.contactRepository.Delete(Arg.IsAny<int>())).DoInstead((int arg) =>
            {
                actual = arg;
            }).Returns(true);
            mocker.MockContactRepository();
            ContactController controller = new ContactController(mocker.unitOfWork);

            // Act
            var result = controller.Delete(1) as RedirectToRouteResult;


            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Index", result.RouteValues["action"]);
            Assert.AreEqual(expected.contact_id, actual);
        }

        [TestMethod]
        public void TestDeleteNoSuchContact()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            Mock.Arrange(() => mocker.contactRepository.Delete(Arg.IsAny<int>())).Returns(false);
            mocker.MockContactRepository();
            ContactController controller = new ContactController(mocker.unitOfWork);

            // Act
            var actual = controller.Delete(1) as HttpNotFoundResult;


            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(404, actual.StatusCode);
        }

        [TestMethod]
        public void TestisUserInContact()
        {
            // Arrange
            // Setup current user UserA, UserB who is not in contact list, UserC who is in contact, UserD who is in contact with userc
            var userA = new User { userId = 1 };
            var userB = new User { userId = 2 };
            var userC = new User { userId = 3 };
            var userD = new User { userId = 4 };

            var contactAToB = new Contact { primary_userId = 1, secondary_userId = 2 };
            var contactDtoC = new Contact { primary_userId = 4, secondary_userId = 3 };
            userA.Contacts = new List<Contact> { contactAToB };
            userB.Contacts1 = new List<Contact> { contactAToB };
            userC.Contacts1 = new List<Contact> { contactDtoC };
            userD.Contacts = new List<Contact> { contactDtoC };
            var allContacts = new List<Contact> { contactAToB, contactDtoC };

            ContactController controller = new ContactController();

            // Act
            var actual = controller.isUserInContact(2, allContacts);
            var actual2 = controller.isUserInContact(5, allContacts);

            // Assert
            Assert.AreEqual(true, actual);
            Assert.AreEqual(false, actual2);
        }
    }
}
