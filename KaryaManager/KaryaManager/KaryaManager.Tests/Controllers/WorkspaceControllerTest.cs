﻿using KaryaManager.Controllers;
using KaryaManager.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Telerik.JustMock;
using KaryaManager.DAL;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web.Security;
using KaryaManager.Tests.MockHelper;

namespace KaryaManager.Tests.Controllers
{
    [TestClass]
    public class WorkspaceControllerTest
    {
        [TestMethod]
        public void TestIndex()
        {
            // Arrange
            WorkspaceController controller = new WorkspaceController();

            // Act
            var actual = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void TestContact()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var expected = new Contact();
            var contacts = new List<Contact> { expected };
            var user = new User { Contacts = contacts };
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);
            Mock.Arrange(() => mocker.userRepository.GetByID(Arg.IsAny<int>())).Returns(user);
            mocker.MockUserRepository();
            WorkspaceController controller = new WorkspaceController(mocker.unitOfWork, webSecurity);

            // Act
            var result = controller.Contact() as PartialViewResult;
            var actual = (List<Contact>)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(expected, actual[0]);
        }

        [TestMethod]
        public void TestProject()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var expected = new Project();
            var projects = new List<Project> { expected };
            var user = new User { Projects = projects };
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);
            Mock.Arrange(() => mocker.userRepository.GetByID(Arg.IsAny<int>())).Returns(user);
            mocker.MockUserRepository();
            WorkspaceController controller = new WorkspaceController(mocker.unitOfWork, webSecurity);

            // Act
            var result = controller.Project() as PartialViewResult;
            var actual = (List<Project>)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(expected, actual[0]);
        }

        [TestMethod]
        public void TestTeamProject()
        {
            // Arrange
            MockCreator mocker = new MockCreator();
            var expected = new Project { projectId = 3 };
            var project_user = new Project_user { projectId = 3 };
            var project_users = new List<Project_user> { project_user };
            var user = new User { Project_user = project_users };
            var webSecurity = Mock.Create<WebSecurityWrapper>();
            Mock.Arrange(() => webSecurity.CurrentUserId).Returns(1);
            var projectRepository = Mock.Create<GenericRepository<Project>>();
            Mock.Arrange(() => mocker.userRepository.GetByID(Arg.IsAny<int>())).Returns(user);
            Mock.Arrange(() => projectRepository.GetByID(Arg.IsAny<int>())).Returns(expected);
            Mock.Arrange(() => mocker.unitOfWork.ProjectRepository).Returns(projectRepository);
            mocker.MockUserRepository();
            WorkspaceController controller = new WorkspaceController(mocker.unitOfWork, webSecurity);

            // Act
            var result = controller.TeamProject() as PartialViewResult;
            var actual = (List<Project>)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(expected, actual[0]);
        }

    }
}
