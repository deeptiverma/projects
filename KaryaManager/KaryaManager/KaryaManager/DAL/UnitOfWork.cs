﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KaryaManager.Models;

namespace KaryaManager.DAL
{
    public class UnitOfWork : IDisposable
    {
        private KaryaDBContext context = new KaryaDBContext();
        private GenericRepository<Project> projectRepository;
        private GenericRepository<User> userRepository;
        private GenericRepository<Task> taskRepository;
        private GenericRepository<Contact> contactRepository;
        private GenericRepository<Project_user> projectUserRepository;
        private bool disposed = false;

        public GenericRepository<Project> ProjectRepository
        {
            get
            {

                if (this.projectRepository == null)
                {
                    this.projectRepository = new GenericRepository<Project>(context);
                }
                return projectRepository;
            }
        }

        public GenericRepository<User> UserRepository
        {
            get
            {

                if (this.userRepository == null)
                {
                    this.userRepository = new GenericRepository<User>(context);
                }
                return userRepository;
            }
        }

        public GenericRepository<Task> TaskRepository
        {
            get
            {

                if (this.taskRepository == null)
                {
                    this.taskRepository = new GenericRepository<Task>(context);
                }
                return taskRepository;
            }
        }

        public GenericRepository<Contact> ContactRepository
        {
            get
            {

                if (this.contactRepository == null)
                {
                    this.contactRepository = new GenericRepository<Contact>(context);
                }
                return contactRepository;
            }
        }

        public GenericRepository<Project_user> ProjectUserRepository
        {
            get
            {

                if (this.projectUserRepository == null)
                {
                    this.projectUserRepository = new GenericRepository<Project_user>(context);
                }
                return projectUserRepository;
            }
        }

        public void Commit()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}