﻿using KaryaManager.Models;
using System.Data;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;

namespace KaryaManager.DAL
{
    public interface IGenericRepository<Entity>
    {
        IEnumerable<Entity> Get(Expression<Func<Entity, bool>> filter,
        Func<IQueryable<Entity>, IOrderedQueryable<Entity>> orderBy,
        string includePropertie);
        Entity GetByID(object id);
        void Insert(Entity entity);
        Boolean Delete(object id);
        void Delete(Entity entityToDelete);
        void Update(Entity entityToUpdate);
    }
}
