﻿using KaryaManager.Models;
using System.Data;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;

namespace KaryaManager.DAL
{
    public class GenericRepository<Entity> : IGenericRepository<Entity> where Entity : class
    {
        private KaryaDBContext context;
        private DbSet dbSet;

        public GenericRepository(KaryaDBContext context)
        {
            this.context = context;
            this.dbSet = context.Set<Entity>();
        }

        public virtual IEnumerable<Entity> Get(
            Expression<Func<Entity, bool>> filter = null,
            Func<IQueryable<Entity>, IOrderedQueryable<Entity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Entity> query = (IQueryable<Entity>)dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual Entity GetByID(object id)
        {
            return (Entity)dbSet.Find(id);
        }

        public virtual void Insert(Entity entity)
        {
            dbSet.Add(entity);
        }

        public virtual Boolean Delete(object id)
        {
            Entity entityToDelete = (Entity)dbSet.Find(id);
            if (entityToDelete == null)
                return false;
            Delete(entityToDelete);
            return true;
        }

        public virtual void Delete(Entity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(Entity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}