﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KaryaManager.Models
{
    [MetadataType(typeof(ProjectMetaData))]
    public partial class Project
    {
        internal sealed class ProjectMetaData
        {
            [DisplayName("Project Name")]
            [MaxLength(50, ErrorMessage = "Project name is too long")]
            [Required(ErrorMessage = "Name is required")]
            public string projectName { get; set; }
            [DisplayName("Project Description")]
            [Required(ErrorMessage = "Description is required")]
            public string projectDescription { get; set; }
            [DisplayName("Status")]
            public string projectStatus { get; set; }
        }
    }
}