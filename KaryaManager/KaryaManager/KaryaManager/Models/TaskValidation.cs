﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KaryaManager.Models
{
    [MetadataType(typeof(TaskMetaData))]
    public partial class Task
    {
        internal sealed class TaskMetaData
        {
            [DisplayName("Task Name")]
            [MaxLength(50, ErrorMessage = "Entry is too long")]
            [Required]
            public string taskName { get; set; }
            [DisplayName("Task description")]
            [Required]
            [MaxLength(50, ErrorMessage = "Entry is too long")]
            public string taskDescription { get; set; }
            [DisplayName("Status")]
            public string taskStatus { get; set; }
            [DisplayName("Assignee")]
            public Nullable<int> assignee { get; set; }
            [DisplayName("Start Date")]
            public Nullable<System.DateTime> start_date { get; set; }
            [DisplayName("Estimated Hours")]
            public Nullable<int> estimate_hour { get; set; }
            [DisplayName("Worked Hours")]
            public Nullable<int> worked_hour { get; set; }
            [DisplayName("Priority")]
            [Range(0, 5, ErrorMessage = "Priority must be between 0 and 5")]
            public Nullable<int> priority { get; set; }
            [DisplayName("Progress")]
            [Range(0, 100, ErrorMessage = "Progress must be between 0 and 100")]
            public Nullable<int> progress { get; set; }
        }
    }
}