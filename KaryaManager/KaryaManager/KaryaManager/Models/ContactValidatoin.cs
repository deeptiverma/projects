﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KaryaManager.Models
{
    [MetadataType(typeof(ContactMetaData))]
    public partial class Contact
    {
        internal sealed class ContactMetaData
        {
            [Required(ErrorMessage = "A contact is required")]
            public int secondary_userId { get; set; }
        }
    }
}