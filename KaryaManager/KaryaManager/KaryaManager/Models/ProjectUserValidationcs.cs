﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KaryaManager.Models
{
    [MetadataType(typeof(Project_userMetaData))]
    public partial class Project_user
    {
        internal sealed class Project_userMetaData
        {
            [Required(ErrorMessage = "A contact is required")]
            public int userId { get; set; }
        }
    }
}