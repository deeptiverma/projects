﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KaryaManager.Models
{
    public static class CurrentRole
    {
        public static string leaderRole { get; set; }
        public static string memberRole { get; set; }
        public static string roles { get; set; }

        public static void SetProject(int projectId)
        {
            leaderRole = projectId + " Leader";
            memberRole = projectId + " Member";
            roles = leaderRole + ", " + memberRole;
        }
    }
}