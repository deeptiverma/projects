﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Reflection;

namespace KaryaManager.Models
{
    public enum TaskStatus
    {
        Open,
        [Description("In Progress")]
        InProgress,
        Closed
    }
}