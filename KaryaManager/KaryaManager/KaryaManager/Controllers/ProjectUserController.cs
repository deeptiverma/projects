﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KaryaManager.Models;
using KaryaManager.DAL;
using WebMatrix.WebData;
using System.Web.Security;
using KaryaManager.CustomWebSecurityWrapper;

namespace KaryaManager.Controllers
{
    [Authorize]
    public class ProjectUserController : Controller
    {
        private KaryaDBContext db = new KaryaDBContext();
        private UnitOfWork unitOfWork;
        private WebSecurityWrapper webSecurity;

        public ProjectUserController()
        {
            unitOfWork = new UnitOfWork();
            webSecurity = new WebSecurityWrapper();
        }

        public ProjectUserController(UnitOfWork uw = null, WebSecurityWrapper wr = null)
        {
            if (uw == null)
                unitOfWork = new UnitOfWork();
            else
                unitOfWork = uw;
            if(wr == null)
                webSecurity = new WebSecurityWrapper();
            else
                this.webSecurity = wr;
        }

        //
        // GET: /ProjectUser/
        [ChildActionOnly]
        public ActionResult Index(int id = 0)
        {
            var project_user = unitOfWork.ProjectRepository.GetByID(id).Project_user;
            return PartialView(project_user);
        }

        //
        // GET: /ProjectUser/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    Project_user project_user = db.Project_user.Find(id);
        //    if (project_user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(project_user);
        //}

        //
        // GET: /ProjectUser/Create

        public ActionResult Create(int id = 0)
        {
            Project_user project_user = new Project_user();
            project_user.projectId = id;
            ViewBag.userId = new SelectList(getUserInContact(id), "userId", "userName");
            return PartialView(project_user);
        }

        //
        // POST: /ProjectUser/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Project_user project_user)
        {
            if (ModelState.IsValid)
            {
                AddUserToTeamRole(project_user.projectId, unitOfWork.UserRepository.GetByID(project_user.userId).userName);
                unitOfWork.ProjectUserRepository.Insert(project_user);
                unitOfWork.Commit();
                //db.Project_user.Add(project_user);
                //db.SaveChanges();
                TempData["message"] = "Team member added successfully.";
                return RedirectToAction("Index", "Dashboard", new { id = project_user.projectId } );
            }
            ViewBag.userId = new SelectList(getUserInContact(project_user.projectId), "userId", "userName");
            return PartialView(project_user);
        }

        ////
        //// GET: /ProjectUser/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    Project_user project_user = unitOfWork.ProjectUserRepository.GetByID(id);
        //    if (project_user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.userId = new SelectList(getUserInContact(project_user.projectId), "userId", "userName", project_user.userId);
        //    return View(project_user);
        //}

        ////
        //// POST: /ProjectUser/Edit/5

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(Project_user project_user)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        unitOfWork.ProjectUserRepository.Update(project_user);
        //        unitOfWork.Commit();
        //       // db.Entry(project_user).State = EntityState.Modified;
        //       // db.SaveChanges();
        //        TempData["message"] = "Team member updated successfully.";
        //        return RedirectToAction("Index", "Dashboard", project_user.projectId);
        //    }
        //    ViewBag.userId = new SelectList(getUserInContact(project_user.projectId), "userId", "userName", project_user.userId);
        //    return View(project_user);
        //}


        //
        // GET: /ProjectUser/Delete/5

        public ActionResult Delete(int id = 0)
        {
            try
            {
                Project_user project_user = unitOfWork.ProjectUserRepository.GetByID(id);
                if (project_user == null)
                {
                    return HttpNotFound();
                }
                int projectId = project_user.projectId;
                RemoveUserFromTeamRole(projectId, project_user.User.userName);
                unitOfWork.ProjectUserRepository.Delete(id);
                unitOfWork.Commit();
                TempData["message"] = "User removed successfully.";
                return RedirectToAction("Index", "Dashboard", new { id = projectId });

            }
            catch
            {
                TempData["error"] = "Error removing user.";
                return null;
            }
        }

        //
        // POST: /ProjectUser/Delete/5

        //[HttpPost, ActionName("DeleteConfirmed")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Project_user project_user = unitOfWork.ProjectUserRepository.GetByID(id);
        //    int projectId = project_user.projectId;
        //    RemoveUserFromTeamRole(projectId, project_user.User.userName);
        //    unitOfWork.ProjectUserRepository.Delete(id);
        //    unitOfWork.Commit();
        //    //db.Project_user.Remove(project_user);
        //    //db.SaveChanges();
        //    TempData["message"] = "Team member deleted successfully.";
        //    return RedirectToAction("Index", "Dashboard", new { id = projectId });
        //}

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        [NonAction]
        public List<User> getUserInContact(int projectId)
        {
            ICollection<Contact> contacts = unitOfWork.UserRepository.GetByID(webSecurity.CurrentUserId).Contacts;
            List<User> userInProject = getUserInProject(projectId);
            List<User> users = new List<User>();
            foreach (Contact contact in contacts)
            {
                if(!userInProject.Contains(contact.User1))
                    users.Add(contact.User1);
            }
            return users;
        }

        [NonAction]
        public List<User> getUserInProject(int? projectId)
        {
            if (projectId == null)
                return null;
            ICollection<Project_user> project_users = unitOfWork.ProjectRepository.GetByID(projectId).Project_user;
            List<User> users = new List<User>();
            foreach (Project_user pu in project_users)
            {
                users.Add(pu.User);
            }
            return users;
        }

        [NonAction]
        public void AddUserToTeamRole(int projectId, String userName)
        {
            String memberRole = projectId + " Member";
            Roles.AddUserToRole(userName, memberRole);
        }

        [NonAction]
        public void RemoveUserFromTeamRole(int projectId, String userName)
        {
            String memberRole = projectId + " Member";
            Roles.RemoveUserFromRole(userName, memberRole);
        }
    }
}