﻿using KaryaManager.DAL;
using KaryaManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace KaryaManager.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private UnitOfWork unitOfWork;

        public DashboardController()
        {
            unitOfWork = new UnitOfWork();
        }

        public DashboardController(UnitOfWork uw = null)
        {
            if (uw == null)
                unitOfWork = new UnitOfWork();
            else
                unitOfWork = uw;
        }

        //
        // GET: /Dashboard/
        public ActionResult Index(int id)
        {
            CurrentRole.SetProject(id);
            if(!(User.IsInRole(CurrentRole.leaderRole) || User.IsInRole(CurrentRole.memberRole)))
                return HttpNotFound();
            if(User.IsInRole(CurrentRole.memberRole))
                return RedirectToAction("TaskDashboard", new { id = id });
            ViewBag.projectId = id;
            return View();
        }

        [ChildActionOnly]
        public ActionResult ProjectDashboard(int id)
        {
            Project project = unitOfWork.ProjectRepository.GetByID(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            ViewBag.taskStatus = getCurrentTaskStatus(project);
            ViewBag.taskPriority = getCurrentTaskPriorities(project);
            return PartialView(project);
        }

        public ActionResult TaskDashboard(int id)
        {
            if(!(User.IsInRole(CurrentRole.leaderRole) || User.IsInRole(CurrentRole.memberRole)))
                return HttpNotFound();

            Project project = unitOfWork.ProjectRepository.GetByID(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            ViewBag.team_members = getUserInProject(id);
            ViewBag.projectId = id;
            ViewBag.task_statuses = getTaskStatuses();
            ViewBag.projectStatus = project.projectStatus;
            return View(project.Tasks);
        }

        [NonAction]
        public List<String> getTaskStatuses()
        {
            List<String> ret = new List<String>();
            foreach (TaskStatus status in Enum.GetValues(typeof(TaskStatus)))
                ret.Add(EnumExtension.GetEnumDescription(status));
            return ret;
        }

        [NonAction]
        public List<User> getUserInProject(int? projectId)
        {
            if (projectId == null)
                return null;
            ICollection<Project_user> project_users = unitOfWork.ProjectRepository.GetByID(projectId).Project_user;
            List<User> users = new List<User>();
            foreach (Project_user pu in project_users)
            {
                users.Add(pu.User);
            }
            users.Add(unitOfWork.ProjectRepository.GetByID(projectId).User);
            return users;
        }

        [NonAction]
        public int[] getCurrentTaskStatus(Project project)
        {
            String[] statuses = project.Tasks.Select(task => task.taskStatus).ToArray();
            int [] statusCount = new int [3];
            foreach (String status in statuses)
            {
                if (status == EnumExtension.GetEnumDescription(TaskStatus.Open))
                    statusCount[0]++;
                else if (status == EnumExtension.GetEnumDescription(TaskStatus.InProgress))
                    statusCount[1]++;
                else if (status == EnumExtension.GetEnumDescription(TaskStatus.Closed))
                    statusCount[2]++;
            }

            return statusCount;
        }

        [NonAction]
        public int[] getCurrentTaskPriorities(Project project)
        {
            int? [] priority = project.Tasks.Select(task => task.priority).ToArray();
            int[] priorityCount = new int[6];
            foreach (int p in priority)
            {
                priorityCount[p]++;
            }

            return priorityCount;
        }
    }
}
