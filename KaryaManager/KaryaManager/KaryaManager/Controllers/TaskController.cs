﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KaryaManager.Models;
using KaryaManager.DAL;
using WebMatrix.WebData;
using KaryaManager.CustomWebSecurityWrapper;

namespace KaryaManager.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        //private KaryaDBContext db = new KaryaDBContext();
        private UnitOfWork unitOfWork;
        private WebSecurityWrapper webSecurity;

        public TaskController()
        {
            unitOfWork = new UnitOfWork();
            webSecurity = new WebSecurityWrapper();
        }

        public TaskController(UnitOfWork uw = null, WebSecurityWrapper wr = null)
        {
            if (uw == null)
                unitOfWork = new UnitOfWork();
            else
                unitOfWork = uw;
            if(wr == null)
                webSecurity = new WebSecurityWrapper();
            else
                this.webSecurity = wr;
        }

        //
        // GET: /Task/

        public ActionResult Index()
        {
            var tasks = unitOfWork.TaskRepository.Get();
            //var tasks = db.Tasks.Include(t => t.Project).Include(t => t.User);
            return View(tasks);
        }

        //
        // GET: /Task/Details/5

        public ActionResult Details(int id = 0)
        {
            Task task = unitOfWork.TaskRepository.GetByID(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        //
        // GET: /Task/Create

        public ActionResult Create(int id = 0)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole) || User.IsInRole(CurrentRole.memberRole)))
                return HttpNotFound();
            ViewBag.assignee = new SelectList(getUserInProject(id), "userId", "userName");
            ViewBag.task_statuses = getTaskStatuses();
            Task task = new Task();
            task.projectId = id;
            task.progress = 0;
            task.priority = 0;
            task.taskStatus = EnumExtension.GetEnumDescription(TaskStatus.Open);
           // ViewBag.assignee = new SelectList(db.Users, "userId", "userName");
            return PartialView(task);
        }

        //
        // POST: /Task/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Task task)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.TaskRepository.Insert(task);
                unitOfWork.Commit();
                //db.Tasks.Add(task);
               // db.SaveChanges();
                TempData["message"] = "Task created successfully.";
                return RedirectToAction("TaskDashboard", "Dashboard", new { id = task.projectId });
            }
            ViewBag.assignee = new SelectList(getUserInProject(task.projectId), "userId", "userName");
            ViewBag.task_statuses = getTaskStatuses();
            return PartialView(task);
        }

        //
        // GET: /Task/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole) || User.IsInRole(CurrentRole.memberRole)))
                return HttpNotFound();
            Task task = unitOfWork.TaskRepository.GetByID(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            ViewBag.assignee = new SelectList(getUserInProject(task.projectId), "userId", "userName", task.assignee);
            ViewBag.task_statuses = getTaskStatuses();
            return View(task);
        }

        //
        // POST: /Task/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Task task)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.TaskRepository.Update(task);
                unitOfWork.Commit();
                //db.Entry(task).State = EntityState.Modified;
                //db.SaveChanges();
                TempData["message"] = "Task updated successfully.";
                return RedirectToAction("TaskDashboard", "Dashboard", new { id = task.projectId });
            }
            ViewBag.assignee = new SelectList(getUserInProject(task.projectId), "userId", "userName", task.assignee);
            ViewBag.task_statuses = getTaskStatuses();
            return View(task);
        }

        public ActionResult Delete(int id)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole) || User.IsInRole(CurrentRole.memberRole)))
                return HttpNotFound();
            try
            {
                Task task = unitOfWork.TaskRepository.GetByID(id);
                int projectId = (int)task.projectId;
                unitOfWork.TaskRepository.Delete(id);
                unitOfWork.Commit();
                TempData["message"] = "Task deleted successfully.";
                return RedirectToAction("TaskDashboard", "Dashboard", new { id = projectId });
            }
            catch
            {
                TempData["error"] = "Error deleting task.";
                return null;
            }
        }

        //
        // POST: /Task/Delete/5

        //[HttpPost, ActionName("DeleteConfirmed")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Task task = unitOfWork.TaskRepository.GetByID(id);
        //    int projectId = (int)task.projectId;
        //    unitOfWork.TaskRepository.Delete(id);
        //    unitOfWork.Commit();
        //    //db.Tasks.Remove(task);
        //    //db.SaveChanges();
        //    return RedirectToAction("TaskDashboard", "Dashboard", new { id = projectId });
        //}

        [HttpPost]
        public ActionResult UpdateStatus(String status, int taskId)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole) || User.IsInRole(CurrentRole.memberRole)))
                return HttpNotFound();
            Task task = unitOfWork.TaskRepository.GetByID(taskId);
            task.taskStatus = status;
            unitOfWork.TaskRepository.Update(task);
            unitOfWork.Commit();
            return null;
        }

        [HttpPost]
        public ActionResult UpdateUser(int? assignee, int taskId)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole) || User.IsInRole(CurrentRole.memberRole)))
                return HttpNotFound();
            Task task = unitOfWork.TaskRepository.GetByID(taskId);
            task.assignee = assignee;
            unitOfWork.TaskRepository.Update(task);
            unitOfWork.Commit();
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            //db.Dispose();
            base.Dispose(disposing);
        }

        [NonAction]
        public List<User> getUserInProject(int? projectId)
        {
            if (projectId == null)
                return null;
            ICollection<Project_user> project_users = unitOfWork.ProjectRepository.GetByID(projectId).Project_user;
            List<User> users = new List<User>();
            foreach (Project_user pu in project_users)
            {
                users.Add(pu.User);
            }
            users.Add(unitOfWork.ProjectRepository.GetByID(projectId).User);
            return users;
        }

        [NonAction]
        public List<String> getTaskStatuses()
        {
            List<String> ret = new List<String>();
            foreach (TaskStatus status in Enum.GetValues(typeof(TaskStatus)))
                ret.Add(EnumExtension.GetEnumDescription(status));
            return ret;
        }
    }
}