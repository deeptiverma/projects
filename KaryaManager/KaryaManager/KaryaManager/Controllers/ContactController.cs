﻿using KaryaManager.DAL;
using KaryaManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KaryaManager.CustomWebSecurityWrapper;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace KaryaManager.Controllers
{
    public class ContactController : Controller
    {
        private UnitOfWork unitOfWork;
        private WebSecurityWrapper webSecurity;

        public ContactController()
        {
            unitOfWork = new UnitOfWork();
            webSecurity = new WebSecurityWrapper();
        }

        public ContactController(UnitOfWork uw = null, WebSecurityWrapper wr = null)
        {
            if (uw == null)
                unitOfWork = new UnitOfWork();
            else
                unitOfWork = uw;
            if(wr == null)
                webSecurity = new WebSecurityWrapper();
            else
                this.webSecurity = wr;
        }


        //
        // GET: /Contact/

        public ActionResult Index()
        {
            User user = unitOfWork.UserRepository.GetByID(webSecurity.CurrentUserId);
            return View(user.Contacts);
        }

        public ActionResult Create()
        {
            // Get ALL user except the current user
            IEnumerable<User> users = unitOfWork.UserRepository.Get();
            users = users.Where(usr => usr.userId != webSecurity.CurrentUserId);

            // Get all contact belongs to the current user
            IEnumerable<Contact> contacts = unitOfWork.ContactRepository.Get();
            contacts = contacts.Where(usr => usr.primary_userId == webSecurity.CurrentUserId);

            // Filter user to get the user who are Not in the current user contact list
            users = users.Where(usr => !isUserInContact(usr.userId, contacts));
            ViewBag.secondary_user = new SelectList(users, "userId", "userName");
            return PartialView(new Contact());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Contact contact)
        {

            if (ModelState.IsValid)
            {
                contact.primary_userId = webSecurity.CurrentUserId;
                unitOfWork.ContactRepository.Insert(contact);
                unitOfWork.Commit();
                TempData["message"] = "Contact added successfully.";
                return RedirectToAction("Index", "Workspace");
                // return RedirectToAction("Index");

            }
            return PartialView(contact);
        }

        public ActionResult Delete(int id = 0)
        {
            if (!unitOfWork.ContactRepository.Delete(id))
            {
                return HttpNotFound();
            }
            unitOfWork.Commit();
            TempData["message"] = "Contact deleted successfully.";
            return RedirectToAction("Index");
        }

        [NonAction]
        public Boolean isUserInContact(int userId, IEnumerable<Contact> contacts)
        {
            foreach (Contact contact in contacts)
            {
                if (contact.secondary_userId == userId)
                    return true;
            }
            return false;
        }

    }
}
