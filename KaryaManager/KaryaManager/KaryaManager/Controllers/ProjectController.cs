﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KaryaManager.Models;
using KaryaManager.DAL;
using WebMatrix.WebData;
using System.Web.Security;
using KaryaManager.CustomWebSecurityWrapper;

namespace KaryaManager.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private UnitOfWork unitOfWork;
        private WebSecurityWrapper webSecurity;

        public ProjectController()
        {
            unitOfWork = new UnitOfWork();
            webSecurity = new WebSecurityWrapper();
        }

        public ProjectController(UnitOfWork uw = null, WebSecurityWrapper wr = null)
        {
            if (uw == null)
                unitOfWork = new UnitOfWork();
            else
                unitOfWork = uw;
            if(wr == null)
                webSecurity = new WebSecurityWrapper();
            else
                this.webSecurity = wr;
        }

        //
        // GET: /Project/

        public ActionResult Index()
        {
            //var projects = db.Projects.Include(p => p.User);
            var projects = unitOfWork.ProjectRepository.Get();
            return View(projects);
        }

        //
        // GET: /Project/Details/5

        public ActionResult Details(int id = 0)
        {
            Project project = unitOfWork.ProjectRepository.GetByID(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        //
        // GET: /Project/Create
        public ActionResult Create()
        {
            Project project = new Project();
            ViewBag.project_statuses = getProjectStatuses();
            return PartialView(project);
        }

        //
        // POST: /Project/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Project project)
        {
            
            if (ModelState.IsValid)
            {
                project.ownerId = webSecurity.CurrentUserId;
                project.projectStatus = "Open";
                unitOfWork.ProjectRepository.Insert(project);
                unitOfWork.Commit();
                establishProjectRole(project.projectId);
                TempData["message"] = "Project created successfully.";
                return RedirectToAction("Index", "Workspace");
               // return RedirectToAction("Index");
                
            }
            ViewBag.project_statuses = new SelectList(getProjectStatuses());
            return PartialView(project);
            //ViewBag.ownerId = new SelectList(db.Users, "userId", "userName", project.ownerId);
            //return View(project);
        }

        //
        // GET: /Project/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole)))
                return HttpNotFound();
            Project project = unitOfWork.ProjectRepository.GetByID(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            ViewBag.project_statuses = getProjectStatuses();
           // ViewBag.ownerId = new SelectList(db.Users, "userId", "userName", project.ownerId);
            return PartialView(project);
        }

        //
        // POST: /Project/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Project project)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.ProjectRepository.Update(project);
                unitOfWork.Commit();
                //db.Entry(project).State = EntityState.Modified;
                //db.SaveChanges();
                TempData["message"] = "Project updated successfully.";
                return RedirectToAction("Index", "Dashboard", new { id = project.projectId } );
            }
            ViewBag.project_statuses = getProjectStatuses();
            //ViewBag.ownerId = new SelectList(db.Users, "userId", "userName", project.ownerId);
            return PartialView(project);
        }

        public ActionResult DeletePrompt(int id)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole)))
                return HttpNotFound();
            try
            {
                unitOfWork.ProjectRepository.Delete(id);
                unitOfWork.Commit();
                deleteProjectRole(id);
                TempData["message"] = "Project deleted successfully.";
                return RedirectToAction("Index", "Workspace");
            }
            catch
            {
                TempData["error"] = "Error deleting task.";
                return RedirectToAction("Index", "Workspace");
            }
        }

        //
        // GET: /Project/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!(User.IsInRole(CurrentRole.leaderRole)))
                return HttpNotFound();
            if(!unitOfWork.ProjectRepository.Delete(id))
            {
                return HttpNotFound();
            }
            deleteProjectRole(id);
            unitOfWork.Commit();
            TempData["message"] = "Project deleted successfully.";
            return RedirectToAction("Index", "Workspace");
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        [NonAction]
        public List<String> getProjectStatuses ()
        {
            List<String> ret = new List<String>();
            foreach(ProjectStatus status in Enum.GetValues(typeof(ProjectStatus)))
                ret.Add(EnumExtension.GetEnumDescription(status));
            return ret;
        }

        [NonAction]
        public void establishProjectRole(int projectId)
        {
            String leaderRole = projectId + " Leader";
            String memberRole = projectId + " Member";
            Roles.CreateRole(leaderRole);
            Roles.CreateRole(memberRole);
            Roles.AddUserToRole(webSecurity.CurrentUserName, leaderRole);
        }

        [NonAction]
        public void deleteProjectRole(int projectId)
        {
            String leaderRole = projectId + " Leader";
            String memberRole = projectId + " Member";
            Roles.RemoveUsersFromRole(Roles.GetUsersInRole(leaderRole), leaderRole);
            Roles.DeleteRole(leaderRole);
            string[] members = Roles.GetUsersInRole(memberRole);
            if (members.Length >= 1)
                Roles.RemoveUsersFromRole(members, memberRole);
            Roles.DeleteRole(memberRole);
        }
    }
}