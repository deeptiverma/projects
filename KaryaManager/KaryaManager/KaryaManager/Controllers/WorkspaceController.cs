﻿using KaryaManager.Models;
using KaryaManager.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using KaryaManager.CustomWebSecurityWrapper;

namespace KaryaManager.Controllers
{
    [Authorize]
    public class WorkspaceController : Controller
    {
        private UnitOfWork unitOfWork;
        private WebSecurityWrapper webSecurity;

        public WorkspaceController()
        {
            unitOfWork = new UnitOfWork();
            webSecurity = new WebSecurityWrapper();
        }

        public WorkspaceController(UnitOfWork uw = null, WebSecurityWrapper wr = null)
        {
            if (uw == null)
                unitOfWork = new UnitOfWork();
            else
                unitOfWork = uw;
            if(wr == null)
                webSecurity = new WebSecurityWrapper();
            else
                this.webSecurity = wr;
        }

        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult Contact()
        {
            User user = unitOfWork.UserRepository.GetByID(webSecurity.CurrentUserId);
            return PartialView(user.Contacts);
        }

        [ChildActionOnly]
        public ActionResult Project()
        {
            User user = unitOfWork.UserRepository.GetByID(webSecurity.CurrentUserId);
            return PartialView(user.Projects);
        }

        [ChildActionOnly]
        public ActionResult TeamProject()
        {
            User user = unitOfWork.UserRepository.GetByID(webSecurity.CurrentUserId);
            List<Project> projects = new List<Project>();
            foreach(Project_user pu in user.Project_user)
            {
                projects.Add(unitOfWork.ProjectRepository.GetByID(pu.projectId));
            }
            return PartialView(projects);
        }
    }
}
