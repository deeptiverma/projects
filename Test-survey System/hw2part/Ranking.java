/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hw2part;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Deepti
 */
public class Ranking extends Question
{
    
    String ques;
    Ranking()
    {            
        super();
        ques="Rank the following"+"\n";
    }
    
    @Override
    public void getQues() 
    {  
        Scanner scanner=new Scanner(System.in);
        System.out.println("enter the items to be ranked");
        System.out.println("enter quit when you are done");
        String match1 = scanner.nextLine();
        int i=1;
        ques=ques+i+")"+match1+"\n";    
        while(!match1.equals("quit"))
        {  
            i=i+1;
            match1 = scanner.nextLine();
            if( !match1.equals("quit"))
            {
                ques=ques+i+")"+match1+"\n";
            }
        }
    }

    @Override
    public void modify() 
    {
        getQues();  
    
    }

    @Override
    public String toString() 
    {
       
        return ques;
    }

    @Override
    public Response getCorrectans() 
    {  
        Scanner scanner=new Scanner(System.in);
        System.out.println("enter the ans as 1342"
                           + " and then enter quit");
        String s=scanner.nextLine();
        //user cannot leave the answer null therefor we check if the input is null
        while(s.isEmpty())
              {
                   System.out.println("ans cannot be left empty");
                   s= scanner.nextLine();

              }
                 //we create an object of class response and add the correct ans to the list of type response

        Response r=new Response();
        r.add(s);
        return r;
        
    }
    
    @Override
    public Response getUserans() 
    {  
        Scanner scanner=new Scanner(System.in);
        System.out.println("enter the ans as 1342" +" and then enter quit");
        String s=scanner.nextLine();
        //user cannot leave the answer null therefor we check if the input is null
        while(s.isEmpty())
              {
                   System.out.println("ans cannot be left empty");
                   s= scanner.nextLine();

        }
           //we create an object of class response and add the correct ans to the list of type response

        Response r=new Response();
        r.add(s);
        return r;
        
    }

    @Override
    public String[][] tabulate(List<Response> r) 
   {
               
       String[][] td = new String[r.size()][2];
       
       for (int a = 0; a < td.length; a++)
       {
           td[a][0] = convertResponses(r.get(a));
           td[a][1] = "0";
       }
       
       //horizontal 
       for (int i = 0; i < r.size(); i++)
       {
           Response res = r.get(i);
           for (int j = 0; j < td.length; j++)
           {
               if (convertResponses(res).equalsIgnoreCase(td[j][0]))
               {
                   td[j][1] = (Integer.parseInt(td[j][1])+1)+"";
               }
           }
       }
       return td;
   }
     private String convertResponses(Response r)
        {
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<r.size();i++)
        {
            sb.append(r.get(i)).append(" ");
        
        }
        return sb.toString();
        }
}
